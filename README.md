# ReinforcementLearning

This repository provides the three assignments completed as part of a Master's coursework in Reinforcement Learning, offered by Prof. Hugo Touchette and Prof. Herman Engelbrecht at Stellenbosch University.

The first assignment, in the form of a notebook, focuses on the k-armed bandit problem, Markov reward process, the Markov decision process, temporal difference, and the Gridworld model. The second assignment is an implementation of Tabular Q-Learning, and the third assignment is an implementation of Deep Q-Networks.

