# Reinforcement Learning Training and Evaluation

This assignment focuses on training and evaluating agents using Deep Q-Network.

## Prerequisites

Before running the training and evaluation files, make sure you have the following dependencies installed:

- hashlib
- numpy
- torch (including the SummaryWriter module)
- pickle
- matplotlib
- minigrid
- gymnasium
- collections



## Training

To train the agents using Deep Q-Network, follow these steps:

1. Make sure the required dependencies are installed.
2. Run the training file `training.py` from your preferred Python environment.
3. The training process will begin, and the policy network will be saved as test.pth.

## Visualization

During the training process, you can use TensorBoard to visualize the training results. Follow these steps:

1. Open a terminal or command prompt.
2. Run the following command to start TensorBoard:
   ```
   tensorboard --logdir=runs
   ```
3. Open your preferred web browser and navigate to the following URL: [http://localhost:6006].
4. The TensorBoard interface will display the training results, which are the reward obtained per episode.

## Evaluation

To evaluate the trained policy network, follow these steps:

1. Ensure that the saved policy network is available in the same working directory as the evaluation file.
2. Run the evaluation file `evaluation.py` from your preferred Python environment.
3. The evaluation process will load the trained policy network and perform the evaluation over 1000 episodes. The completion rate, average rewards, and average steps taken will be displayed

For any questions or issues, please reach out to [22825584@sun.ac.za].
