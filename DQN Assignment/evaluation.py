# import 'gymnasium' and 'minigrid' for our environment
import gym
import minigrid
import math

# import 'random' to generate random numbers
import random

# import 'numpy' for various mathematical, vector and matrix functions
import numpy as np

from os.path import exists

# import 'Pytorch' for all our neural network needs

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
# Import ImgObsWrapper from minigrid module
from minigrid.wrappers import ImgObsWrapper
# if gpu is to be used, otherwise use cpu
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Import 'namedtuple' and 'deque' for Experience Replay Memory
from collections import namedtuple, deque

# define the model
class DQN(nn.Module):

    def __init__(self, inputSize, numActions, hiddenLayerSize=(512, 256)):
        super(DQN, self).__init__()
        self.fc1 = nn.Linear(inputSize, hiddenLayerSize[0])
        self.fc2 = nn.Linear(hiddenLayerSize[0], hiddenLayerSize[1])
        self.fc3 = nn.Linear(hiddenLayerSize[1], numActions)

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        x = x.to(device)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
    
### MODEL HYPERPARAMETERS 
numActions = 3               # 3 possible actions: left, right, move forward
inputSize = 49               # size of the flattened input state (7x7 matrix of tile IDs)

### TRAINING HYPERPARAMETERS
alpha = 0.0002               # learning_rate
episodes = 5000              # Total episodes for training
batch_size = 128             # Neural network batch size

# Q learning hyperparameters
gamma = 0.90                 # Discounting rate

# Exploration parameters for epsilon greedy strategy
start_epsilon = 1.0          # exploration probability at start
stop_epsilon = 0.01          # minimum exploration probability 
decay_rate = 20000          # exponential decay rate for exploration prob

### TESTING HYPERPARAMETERS
# Evaluation hyperparameter
evalEpisodes = 1000          # Number of episodes to be used for evaluation

# Loading a model
filename = 'test.pth'
policy_net = torch.load(filename) 

# set to evaluation mode
policy_net.eval()

def select_action(state):
    # generate a random number
    sample = random.random()

    # calculate the epsilon threshold, based on the epsilon-start value, the epsilon-stop value, 
    # the number of training steps taken, and the epsilon decay rate
    # here we are using an exponential decay rate for the epsilon value
    eps_threshold = stop_epsilon + (start_epsilon - stop_epsilon) * math.exp(-1. * steps_done / decay_rate)
    #eps_threshold  = max(stop_epsilon, start_epsilon*(0.999**steps_done))
    # compare the generated random number to the epsilon threshold
    if sample > eps_threshold:
        # act greedily towards the Q-values of our policy network, given the state

        # we do not want to gather gradients as we are only generating experience, not training the network
        with torch.no_grad():
            # t.max(1) will return the largest column value of each row.
            # The second column on the max result is the index of where the max element was
            # found, so we pick the action with the larger expected reward.
            return policy_net(state).max(1)[1].unsqueeze(0)
    else:
        # select a random action with equal probability
        return torch.tensor([[random.randrange(numActions)]], device=device, dtype=torch.long)
    
# extract the object_idx information as a matrix
def extractObjectInformation(observation):
    (rows, cols, x) = observation.shape
    view = np.zeros((rows, cols))
    for r in range(rows): 
        for c in range(cols): 
            view[r,c] = observation[r,c,0]
    return view

# the following is a more efficient method of extracting the information using numpy slicing and reshaping
def extractObjectInformation2(observation):
    (rows, cols, x) = observation.shape
    tmp = np.reshape(observation,[rows*cols*x,1], 'F')[0:rows*cols]
    return np.reshape(tmp, [rows,cols],'C')

# Normalise the input observation so each element is a scalar value between [0,1]
def normalize(observation, max_value):
    return np.array(observation)/max_value

# Flatten the [7,7] observation matrix into a [1,49] tensor
def flatten(observation):
    return torch.from_numpy(np.array(observation).flatten()).float().unsqueeze(0)

# Combine all the preprocessing fuctions into a single function
def preprocess(observation):
    return flatten(normalize(extractObjectInformation2(observation), 10.0))


# Evaluate the model
# Make the gym environment
env = gym.make('MiniGrid-Empty-8x8-v0')

# Use a wrapper so the observation only contains the grid information
env = ImgObsWrapper(env)

# evaluation loop
finishCounter = 0.0
totalSteps = 0.0
totalReward = 0.0

steps_done = 0
#stop_epsilon = 0.0

for e in range(evalEpisodes):
    # Initialize the environment and state
    currentObs, _ = env.reset()
    currentState = preprocess(currentObs)
   
    # the main RL loop
    for i in range(0, env.max_steps):
        # Select and perform an action
        action = select_action(currentState)
        a = action.item()
        
        # take action 'a', receive reward 'reward', and observe next state 'obs'
        # 'done' indicate if the termination state was reached
        obs, reward, done, truncated, info = env.step(a)
        nextState = preprocess(obs)
        #if (done or truncated):
            # Observe new state
            #nextState = None
        #else:
            #nextState = preprocess(obs)

        if (done or truncated):
            totalReward += reward
            totalSteps += env.step_count
            if (done):
                print('Finished evaluation episode %d with reward %f,  %d steps, reaching goal ' % (e, reward, env.step_count))
                finishCounter += 1
            if (truncated):
                print('Failed evaluation episode %d with reward %f, %d steps' % (e,reward, env.step_count))
            break
        
        # Move to the next state
        currentState = nextState
    steps_done += 1 
# Print a summary of the evaluation results
print('Completion rate %.2f with average reward %0.4f and average steps %0.2f' % (finishCounter/evalEpisodes, totalReward/evalEpisodes,  totalSteps/evalEpisodes))