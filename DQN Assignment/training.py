# import 'gymnasium' and 'minigrid' for our environment
import gym
import minigrid
import math

# import 'random' to generate random numbers
import random

# import 'numpy' for various mathematical, vector and matrix functions
import numpy as np

from os.path import exists


# import 'Pytorch' for all our neural network needs

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
# Import ImgObsWrapper from minigrid module
from minigrid.wrappers import ImgObsWrapper
# if gpu is to be used, otherwise use cpu
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Import 'namedtuple' and 'deque' for Experience Replay Memory
from collections import namedtuple, deque

# extract the object_idx information as a matrix
def extractObjectInformation(observation):
    (rows, cols, x) = observation.shape
    view = np.zeros((rows, cols))
    for r in range(rows): 
        for c in range(cols): 
            view[r,c] = observation[r,c,0]
    return view

# the following is a more efficient method of extracting the information using numpy slicing and reshaping
def extractObjectInformation2(observation):
    (rows, cols, x) = observation.shape
    tmp = np.reshape(observation,[rows*cols*x,1], 'F')[0:rows*cols]
    return np.reshape(tmp, [rows,cols],'C')

# Normalise the input observation so each element is a scalar value between [0,1]
def normalize(observation, max_value):
    return np.array(observation)/max_value

# Flatten the [7,7] observation matrix into a [1,49] tensor
def flatten(observation):
    return torch.from_numpy(np.array(observation).flatten()).float().unsqueeze(0)

# Combine all the preprocessing fuctions into a single function
def preprocess(observation):
    return flatten(normalize(extractObjectInformation2(observation), 10.0))

### Neural network model definition
class DQN(nn.Module):

    def __init__(self, inputSize, numActions, hiddenLayerSize=(512, 256)):
        super(DQN, self).__init__()
        self.fc1 = nn.Linear(inputSize, hiddenLayerSize[0])
        self.fc2 = nn.Linear(hiddenLayerSize[0], hiddenLayerSize[1])
        self.fc3 = nn.Linear(hiddenLayerSize[1], numActions)

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        x = x.to(device)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

# Define the hidden layer size and create the policy_net and target_net
#hiddenLayerSize = (128, 128)
policy_net = DQN(inputSize, numActions).to(device)
target_net = DQN(inputSize, numActions).to(device)

# Copy the weights of the policy network to the target network
target_net.load_state_dict(policy_net.state_dict())

def select_action(state):
    # generate a random number
    sample = random.random()

    # calculate the epsilon threshold, based on the epsilon-start value, the epsilon-stop value, 
    # the number of training steps taken, and the epsilon decay rate
    # here we are using an exponential decay rate for the epsilon value
    eps_threshold = stop_epsilon + (start_epsilon - stop_epsilon) * math.exp(-1. * steps_done / decay_rate)

    # compare the generated random number to the epsilon threshold
    if sample > eps_threshold:
        # act greedily towards the Q-values of our policy network, given the state

        # we do not want to gather gradients as we are only generating experience, not training the network
        with torch.no_grad():
            # t.max(1) will return the largest column value of each row.
            # The second column on the max result is the index of where the max element was
            # found, so we pick the action with the larger expected reward.
            return policy_net(state).max(1)[1].unsqueeze(0)
    else:
        # select a random action with equal probability
        return torch.tensor([[random.randrange(numActions)]], device=device, dtype=torch.long)
    
Transition = namedtuple('Transition',
                        ('currentState', 'action', 'nextState', 'reward'))

class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        """Save a transition"""
        # Convert reward to a tensor with shape (1,)
        args = list(args)
        args[-1] = torch.tensor([args[-1]], device=device, dtype=torch.float32)
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

# Instantiate memory
memory = ReplayMemory(memorySize)

## Create the optimizer
optimizer = optim.Adam(policy_net.parameters(), lr=alpha)
## Training of the model
def optimize_model():
    # Check if the replay memory has stored enough experience
    if len(memory) < batch_size:
        return

    # Sample mini-batch
    experience = memory.sample(batch_size)
    batch = Transition(*zip(*experience))

    # Move batch elements to the same device as policy_net
    state_batch = torch.cat(batch.currentState).to(device)
    action_batch = torch.cat(batch.action).to(device)

    # Calculate action-values using policy network
    state_action_values = policy_net(state_batch).gather(1, action_batch).squeeze(1)

    # Calculate TD-targets using target network
    reward_batch = torch.cat(batch.reward).to(device)
    non_final_next_states = torch.cat([s for s in batch.nextState if s is not None]).to(device)
    next_state_values = torch.zeros(batch_size, device=device)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None, batch.nextState)), device=device, dtype=torch.bool)
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    TDtargets = (next_state_values * gamma) + reward_batch

    # Calculate loss
    criterion = nn.MSELoss()
    loss = criterion(state_action_values, TDtargets)

    # Make gradient descent step and update policy network
    optimizer.zero_grad()
    loss.backward()
    for param in policy_net.parameters():
        param.grad.data.clamp_(-1, 1)

    optimizer.step()

### MODEL HYPERPARAMETERS 
numActions = 3               # 3 possible actions: left, right, move forward
inputSize = 49               # size of the flattened input state (7x7 matrix of tile IDs)

### TRAINING HYPERPARAMETERS
alpha = 0.0002               # learning_rate
episodes = 3000              # Total episodes for training
batch_size = 128             # Neural network batch size
target_update = 100       # Number of episodes between updating target network

# Q learning hyperparameters
gamma = 0.90                 # Discounting rate

# Exploration parameters for epsilon greedy strategy
start_epsilon = 1.0          # exploration probability at start
stop_epsilon = 0.01          # minimum exploration probability 
decay_rate = 2000           # exponential decay rate for exploration prob

### MEMORY HYPERPARAMETERS
pretrain_length = batch_size # Number of experiences stored in the Memory when initialized for the first time
memorySize = 500000          # Number of experiences the Memory can keep - 500000

### TESTING HYPERPARAMETERS
# Evaluation hyperparameter
evalEpisodes = 1000          # Number of episodes to be used for evaluation

# Change this to 'False' if you only want to evaluate a previously trained agent
train = True                 # Specify True to train a model, otherwise only evaluate

# Tensorboard writer
writer = SummaryWriter()
# Set the target network to evaluation mode
target_net.eval()

# Make the gym environment
env = gym.make('MiniGrid-Empty-8x8-v0')

# Use a wrapper so the observation only contains the grid information
env = ImgObsWrapper(env)

episodes = 3000               # total number of training episodes
max_steps = env.max_steps  # maximum number of steps allowed before truncating episode
steps_done = 0             # total training steps taken

# training
print('Start training...')
for e in range(episodes):
    
    # reset the environment
    obs, _ = env.reset()

    # extract the current state from the observation
    state = preprocess(obs)
    
    for i in range(0, max_steps):

        # Choose an action
        # Pick a random action
        action = select_action(state)
        a = action.item()
        
        # take action 'a', receive reward 'reward', and observe next state 'obs'
        # 'done' indicate if the termination state was reached
        obs, reward, done, truncated, info = env.step(a)
   
        # extract the next state from the observation
        nextState = preprocess(obs)
        
        # if the episode is finished, the nextState is set to None to indicate that the
        # <s,a,r,s'> transition led to a terminating state
        if (done or truncated):
            nextState = None
        
        # Store the transition <s,a,r,s'> in the replay memory
        memory.push(state,action,nextState, reward)
        # Move to the next state          
        currentState = nextState

        # Perform one step of the optimization (on the policy network) by
        # sample a mini-batch and train the model using the sampled mini-batch
        optimize_model()
        
        # If the target update threshold is reached, update the target network, 
        # copying all weights and biases in the policy network   
        if steps_done % target_update == 0:
            print("updating network")
            target_net.load_state_dict(policy_net.state_dict())
    
        
        # Episode finished when done or truncated is true
        if (done or truncated):
            # Record the reward and total training steps taken
            if (done):
                # if agent reached its goal successfully
                print('Finished episode successfully taking %d steps and receiving reward %f' % (env.step_count, reward))
            else:
                # agent failed to reach its goal successfully 
                print('Truncated episode taking %d steps and receiving reward %f' % (env.step_count, reward))
            break
            
    steps_done += 1   
    writer.add_scalar("Reward/train", reward, steps_done)  
print('Done training...')

# save the model
filename = 'test.pth'

# Saving a model
torch.save(policy_net, filename)