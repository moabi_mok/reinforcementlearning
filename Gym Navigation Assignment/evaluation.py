# import statements
import hashlib
import numpy as np
from torch.utils.tensorboard import SummaryWriter
import pickle
from os.path import exists
import matplotlib.pyplot as plt
import time
from minigrid.wrappers import *
import gymnasium as gym
import minigrid
import random

# extract the object_idx information as a matrix
def extractObjectInformation(observation):
    (rows, cols, x) = observation.shape
    view = np.zeros((rows, cols))
    for r in range(rows): 
        for c in range(cols): 
            view[r,c] = obs[r,c,0]
    return view

#  more efficient method of extracting the information using numpy slicing & reshaping
def extractObjectInformation2(observation):
    (rows, cols, x) = obs.shape
    tmp = np.reshape(obs,[rows*cols*x,1], 'F')[0:rows*cols]
    return np.reshape(tmp, [rows,cols],'C')

# Combine all the preprocessing fuctions into a single function
def preprocess(observation):
    return extractObjectInformation2(observation)

# define the hash function
def hashState(state):
    hash_value = ""
    nrows = state.shape[0]
    ncolumns = state.shape[1]
    for row in range(nrows):
        for column in range(ncolumns):
            hash_value += str(int(state[row,column]))
    
    return hash_value

# Loading the Tabular Q-learning value-function and Evaluating trained agent
    
# Loading the value-function from file
filename_qlearning = 'qtable_qlearning.pickle'
if (exists(filename_qlearning)):
    print('Loading existing Q values')
    # Load data (deserialize)
    with open(filename_qlearning, 'rb') as handle:
        Q_qlearning = pickle.load(handle)
        handle.close()
else:
    print('Filename %s does not exist, could not load data' % filename_qlearning) 
    
# Evaluation
env = gym.make('MiniGrid-Empty-8x8-v0')
max_steps = env.max_steps
env = ImgObsWrapper(env)
# reset the environment
obs, _ = env.reset()
episodes = 1000
completed_episodes = 0
average_rewards = np.zeros(episodes)
steps_taken = np.zeros(episodes)

for e in range(episodes): 
    # reset the environment
    obs, _ = env.reset()
    # extract the current state from the observation
    currentS = extractObjectInformation2(obs)
    
    for i in range(max_steps):
        currentS_Hash =  hashState(currentS)
        # Choose an action using the optimal policy
        a = np.argmax(Q_qlearning[currentS_Hash])
        # take action 'a', receive reward 'reward', and observe next state 'obs'
        # 'done' indicate if the termination state was reached
        obs, reward, done, truncated, info = env.step(a)
        # extract the next state from the observation
        nextS = extractObjectInformation2(obs)
        nextS_Hash = hashState(nextS) # hash of next state
        #env.render() # render the environment, this does not work inside Jupyter notebook
        # sleep for 50 milliseconds so we can see the rendering of the environment. 
        #time.sleep(0.05) # When training without rendering remove this line
        average_rewards[e] = average_rewards[e] + reward
        steps_taken[e] = steps_taken[e] + 1
        if (done == True):
            # if agent reached its goal successfully
            completed_episodes += 1
            #print('Finished episode successfully taking %d steps and receiving reward %f' % (i+1, reward))
            break
        if (truncated == True):
            # agent failed to reach its goal successfully 
            #print('Truncated episode taking %d steps and receiving reward %f' % (i+1, reward))
            break
            
        # since the episode is not done, store the next state as the current state for the next step
        currentS = nextS

print('The Tabular Q-Learning method best performing agent evaluation results: ')
print('The completion rate is: ',100*completed_episodes/episodes)
# average number of steps
print('The average number of steps is: ',np.mean(steps_taken))
# plot the average reward per training step
print('The average reward is: ',np.mean(average_rewards))

# Loading the SARSA method value-function and Evaluating trained agent

# Loading the value-function from file
filename_sarsa = 'qtable_sarsa.pickle'
if (exists(filename_sarsa)):
    print('Loading existing Q values')
    # Load data (deserialize)
    with open(filename_sarsa, 'rb') as handle:
        Q_sarsa = pickle.load(handle)
        handle.close()
else:
    print('Filename %s does not exist, could not load data' % filename_sarsa) 

# Evaluation - sarsa
env = gym.make('MiniGrid-Empty-8x8-v0')
max_steps = env.max_steps
env = ImgObsWrapper(env)
# reset the environment
obs, _ = env.reset()
episodes = 1000
completed_episodes = 0
average_rewards = np.zeros(episodes)
steps_taken = np.zeros(episodes)

for e in range(episodes): 
    # reset the environment
    obs, _ = env.reset()
    # extract the current state from the observation
    currentS = extractObjectInformation2(obs)
    
    for i in range(max_steps):
        currentS_Hash =  hashState(currentS)
        # Pick  optimal action 
        a = np.argmax(Q_sarsa[currentS_Hash])
        # take action 'a', receive reward 'reward', and observe next state 'obs'
        # 'done' indicate if the termination state was reached
        obs, reward, done, truncated, info = env.step(a)
        # extract the next state from the observation
        nextS = extractObjectInformation2(obs)
        nextS_Hash = hashState(nextS) # hash of next state
        #env.render() # render the environment, this does not work inside Jupyter notebook
        # sleep for 50 milliseconds so we can see the rendering of the environment. 
        #time.sleep(0.05) # When training without rendering remove this line
        average_rewards[e] = average_rewards[e] + reward
        steps_taken[e] = steps_taken[e] + 1
        if (done == True):
            # if agent reached its goal successfully
            completed_episodes += 1
            #print('Finished episode successfully taking %d steps and receiving reward %f' % (i+1, reward))
            break
        if (truncated == True):
            # agent failed to reach its goal successfully 
            #print('Truncated episode taking %d steps and receiving reward %f' % (i+1, reward))
            break
            
        # since the episode is not done, store the next state as the current state for the next step
        currentS = nextS

print('The Tabular SARSA method best performing agent evaluation results: ')
print('The completion rate is: ',100*completed_episodes/episodes)
# average number of steps
print('The average number of steps is: ',np.mean(steps_taken))
# plot the average reward per training step
print('The average reward is: ',np.mean(average_rewards))