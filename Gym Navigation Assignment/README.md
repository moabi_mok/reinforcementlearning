# Reinforcement Learning Training and Evaluation

This assignment focuses on training and evaluating agents using tabular Q-learning and SARSA methods. The training and evaluation files are provided along with the necessary import statements. The report of this assignment is also provided.

## Prerequisites

Before running the training and evaluation files, make sure you have the following dependencies installed:

- hashlib
- numpy
- torch (including the SummaryWriter module)
- pickle
- matplotlib
- minigrid
- gymnasium

You can install the required dependencies using pip:

```
pip install hashlib numpy torch pickle matplotlib minigrid gymnasium
```

## Training

To train the agents using tabular Q-learning and SARSA, follow these steps:

1. Make sure the required dependencies are installed.
2. Run the training file using `python training.py` from the terminal.
3. The training process will begin, and the Q-tables for the best performing agents will be saved as `qtable_qlearning.pickle` for Q-learning's agent and `qtable_sarsa.pickle` for SARSA's agent.

## Visualization

During the training process, you can use TensorBoard to visualize the training results. Follow these steps:

1. Open a terminal or command prompt.
2. Run the following command to start TensorBoard:
   ```
   tensorboard --logdir=runs
   ```
3. Open your preferred web browser and navigate to the following URL: [http://localhost:6006].
4. The TensorBoard interface will display the training results, which are the reward obtained per episode.

## Evaluation

To evaluate the trained agents, follow these steps:

1. Ensure that the value function tables (`qtable_qlearning.pickle` and `qtable_sarsa.pickle`) are available in the same working directory as the evaluation file.
2. Run the evaluation file using  `python evaluation.py` from the terminal.
3. The evaluation process will load the Q-value function tables for both methods and perform the evaluation for each agent.

Note: The evaluation file requires the value function tables generated during the training phase. Please ensure that these files exist in the working directory before running the evaluation.

For any questions or issues, please reach out to [22825584@sun.ac.za].
