# import statements
import hashlib
import numpy as np
from torch.utils.tensorboard import SummaryWriter
import pickle
from os.path import exists
import matplotlib.pyplot as plt
import time
from minigrid.wrappers import *
import gymnasium as gym
import minigrid
import random

# extract the object_idx information as a matrix
def extractObjectInformation(observation):
    (rows, cols, x) = observation.shape
    view = np.zeros((rows, cols))
    for r in range(rows): 
        for c in range(cols): 
            view[r,c] = obs[r,c,0]
    return view

#  more efficient method of extracting the information using numpy slicing & reshaping
def extractObjectInformation2(observation):
    (rows, cols, x) = obs.shape
    tmp = np.reshape(obs,[rows*cols*x,1], 'F')[0:rows*cols]
    return np.reshape(tmp, [rows,cols],'C')

# Combine all the preprocessing fuctions into a single function
def preprocess(observation):
    return extractObjectInformation2(observation)

# define the hash function
def hashState(state):
    hash_value = ""
    nrows = state.shape[0]
    ncolumns = state.shape[1]
    for row in range(nrows):
        for column in range(ncolumns):
            hash_value += str(int(state[row,column]))
    
    return hash_value

# Tabular Q-Learning Training
    
# Tensorboard writer
writer = SummaryWriter()

print('Start training...')
# Make the gym environment
env = gym.make('MiniGrid-Empty-8x8-v0')
# declare the variable to store the tabular value-function
Q = {}
max_steps = env.max_steps
# Use a wrapper so the observation only contains the grid information
env = ImgObsWrapper(env)
# reset the environment
obs, _ = env.reset()
# Set the number of actions
numActions = 3 
# extract the current state from the observation
currentS = extractObjectInformation2(obs)
#number of episodes
episodes = 3000
# Before training
steps_done = 0
#hyperparameters
alpha = 0.3  
gamma = 0.9  
epsilon_max = 0.9  
epsilon_min = 0.01  
epsilon_decay = 0.999 

for e in range(episodes): 
    # reset the environment
    obs, _ = env.reset()
    # extract the current state from the observation
    currentS = extractObjectInformation2(obs)
    # apply epsilon decay
    epsilon = max(epsilon_min, epsilon_max*(epsilon_decay**e))
    
    for i in range(max_steps):
        # initialize value functions
        currentS_Hash =  hashState(currentS)
        if currentS_Hash not in Q:
            Q[currentS_Hash] = np.zeros(numActions)
            
        # chose action using epsilon-greedy approach
        if (random.random() < epsilon):
            # Explore the environment by selecting a random action
            a = random.randint(0, numActions-1)
        else:
            # Exploit the environment by selecting an action that is the maximum of the value function at the current State
            a = np.argmax(Q[currentS_Hash])
            
        # take action 'a', receive reward 'reward', and observe next state 'obs'
        # 'done' indicate if the termination state was reached
        obs, reward, done, truncated, info = env.step(a)
        # extract the next state from the observation
        nextS = extractObjectInformation2(obs)
        nextS_Hash = hashState(nextS) # hash of next state
        
        # initialize next hash 
        if nextS_Hash not in Q:
            Q[nextS_Hash] = np.zeros(numActions)
            
        # update the state-value function
        Q[currentS_Hash][a] +=  alpha*(reward + gamma*np.max(Q[nextS_Hash]) - Q[currentS_Hash][a])
        #env.render() # render the environment, this does not work inside Jupyter notebook
        # sleep for 50 milliseconds so we can see the rendering of the environment. 
        #time.sleep(0.05) # When training without rendering remove this line
        #average_rewards[e] = average_rewards[e] + reward
        #steps_taken[e] = steps_taken[e] + 1
        if (done == True):
            print('Finished episode successfully taking %d steps and receiving reward %f' % (i, reward))
            break
        if (truncated == True):
            # agent failed to reach its goal successfully 
            print('Truncated episode taking %d steps and receiving reward %f' % (i, reward))
            break
            
        # since the episode is not done, store the next state as the current state for the next step
        currentS = nextS
    steps_done += 1   
    writer.add_scalar("Reward/train", reward, steps_done)  
print('End training...')

# Saving the Q-learning value-function
filename_qlearning = 'qtable_qlearning.pickle'
with open(filename_qlearning, 'wb') as handle:
    pickle.dump(Q, handle, protocol=pickle.HIGHEST_PROTOCOL)
    handle.close()

# SARSA method Training
    
# Tensorboard writer
writer = SummaryWriter()
# training
print('Start training ...')
# Make the gym environment
env = gym.make('MiniGrid-Empty-8x8-v0')
# declare the variable to store the tabular value-function
Q_sarsa = {}
max_steps = env.max_steps
# Use a wrapper so the observation only contains the grid information
env = ImgObsWrapper(env)
# reset the environment
obs, _ = env.reset()

# Set the number of actions
numActions = 3  
# extract the current state from the observation
currentS = extractObjectInformation2(obs)
#number of episodes
episodes = 3000
# Before training
steps_done = 0
#hyperparameters
alpha = 0.3  
gamma = 0.9 
epsilon_max = 0.9  
epsilon_min = 0.01  
epsilon_decay = 0.999  

for e in range(episodes): 
    # reset the environment
    obs, _ = env.reset()
    # extract the current state from the observation
    currentS = extractObjectInformation2(obs)
    # initialize value functions
    currentS_Hash =  hashState(currentS)
    if currentS_Hash not in Q_sarsa:
        Q_sarsa[currentS_Hash] = np.zeros(numActions)
    # Pick a random action using the epsilon greedy approach
    if (random.random() < epsilon):
        # Explore the environment by selecting a random action
        a = random.randint(0, numActions-1)
    else:
        # Exploit the environment by selecting an action that is the maximum of the value function at the current State
        a = np.argmax(Q_sarsa[currentS_Hash])

    # epsilon decay
    epsilon = max(epsilon_min, epsilon_max*(epsilon_decay**e))
    
    for i in range(max_steps):
        currentS_Hash =  hashState(currentS)
        # Each time an action is selected, increment the 'steps_done' counter 
        # take action 'a', receive reward 'reward', and observe next state 'obs'
        # 'done' indicate if the termination state was reached
        obs, reward, done, truncated, info = env.step(a)
        # extract the next state from the observation
        nextS = extractObjectInformation2(obs)
        # if nextS isnt in the dictionary yet
        nextS_Hash = hashState(nextS) # hash of next state
        # initialize next value function
        if nextS_Hash not in Q_sarsa:
            Q_sarsa[nextS_Hash] = np.zeros(numActions)
        
        if (random.random() < epsilon):
            # Explore the environment by selecting a random action
            a_next = random.randint(0, numActions-1)
        else:
            # Exploit the environment by selecting an action that is the maximum of the value function at the current State
            a_next = np.argmax(Q_sarsa[nextS_Hash])
            
        # update the state-value function
        Q_sarsa[currentS_Hash][a] +=  alpha*(reward + gamma*Q_sarsa[nextS_Hash][a_next] - Q_sarsa[currentS_Hash][a])
        #env.render() # render the environment, this does not work inside Jupyter notebook

        # sleep for 50 milliseconds so we can see the rendering of the environment. 
        #time.sleep(0.05) # When training without rendering remove this line
        #average_rewards[e] = average_rewards[e] + reward
        #steps_taken[e] = steps_taken[e] + 1
        if (done == True):
            # if agent reached its goal successfully
            print('Finished episode successfully taking %d steps and receiving reward %f' % (i, reward))
            break
        if (truncated == True):
            # agent failed to reach its goal successfully 
            print('Truncated episode taking %d steps and receiving reward %f' % (i, reward))
            break
            
        # since the episode is not done, store the next state as the current state for the next step
        currentS = nextS
        a = a_next
        
    steps_done += 1
    writer.add_scalar("Reward/train", reward, steps_done)   
    
print('End training ...')
writer.flush()
writer.close()

# Saving the SARSA value-function
filename_sarsa = 'qtable_sarsa.pickle'
with open(filename_sarsa, 'wb') as handle:
    pickle.dump(Q_sarsa, handle, protocol=pickle.HIGHEST_PROTOCOL)
    handle.close() 